/* eslint-disable jsx-a11y/no-autofocus */
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { TodoContext } from '../context/todo.context';
import useInputState from '../hooks/useInputState';
import { EDIT } from '../context/types';

function EditForm({ toggle, id, task }) {
  const [value, handleChange, reset] = useInputState('');
  const { dispatch } = useContext(TodoContext);

  return (
    <Form
      onSubmit={e => {
        e.preventDefault();
        dispatch({ type: EDIT, payload: { id, value } });
        reset();
        toggle();
      }}
    >
      <input
        type="text"
        placeholder={task}
        autoFocus
        value={value}
        onChange={handleChange}
      />
    </Form>
  );
}

EditForm.propTypes = {
  toggle: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  task: PropTypes.string.isRequired,
};
const Form = styled.form`
  input {
    padding: 0.6rem 1rem;
    font-size: 1.2rem;
    border: 2px solid #333;
    box-shadow: 1px 2px 3px 2px rgba(3, 3, 3, 0.7);
    transition: all 0.2s ease-in-out;
    border-radius: 1rem;
    &:hover {
      box-shadow: 3px 4px 4px 4px rgba(3, 3, 3, 0.7);
    }
  }
`;
export default EditForm;
