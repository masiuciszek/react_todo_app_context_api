import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

function Navbar(props) {
  return (
    <Nav>
      <h2>Todo App Witch React hook and context API</h2>
    </Nav>
  );
}

Navbar.propTypes = {};

const Nav = styled.nav`
  display: flex;
  justify-content: center;
  background: rgba(4, 4, 4, 0.4);
  color: #fff;
  padding: 2rem;
  font-size: 2.5rem;
  h2 {
    border-bottom: 1px solid #fff;
  }
`;
export default Navbar;
