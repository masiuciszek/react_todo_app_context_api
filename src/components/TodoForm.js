/* eslint-disable jsx-a11y/no-autofocus */
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { TodoContext } from '../context/todo.context';
import useInputState from '../hooks/useInputState';
import { ADD_TODO } from '../context/types';

function TodoForm(props) {
  const [value, handleChange, reset] = useInputState('');
  const { dispatch } = useContext(TodoContext);

  return (
    <Form
      onSubmit={e => {
        e.preventDefault();
        dispatch({ type: ADD_TODO, payload: value });
        reset();
      }}
    >
      <input
        type="text"
        placeholder="add a Todo..."
        autoFocus
        value={value}
        onChange={handleChange}
      />
    </Form>
  );
}

TodoForm.propTypes = {};

const Form = styled.form`
  padding: 1rem;
  text-align: center;
  width: 40%;
  margin: 0 auto;
  margin-top: 3rem;
  input {
    width: 100%;
    padding: 1rem;
    border-radius: 1rem;
    border: 2px solid #333;
    font-size: 1.4rem;
    box-shadow: 1px 2px 3px 2px rgba(3, 3, 3, 0.7);
    transition: all 0.2s ease-in-out;
    &:hover {
      box-shadow: 3px 4px 4px 4px rgba(3, 3, 3, 0.7);
    }
    &:focus {
      border: 2px solid #a537fd;
    }
  }
`;
export default TodoForm;
