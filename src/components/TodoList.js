import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { TodoContext } from '../context/todo.context';
import Todo from './Todo';

function TodoList() {
  const { state } = useContext(TodoContext);

  return (
    <TodoListWrapper>
      {state.map(todo => (
        <Todo key={todo.id} todo={todo} />
      ))}
    </TodoListWrapper>
  );
}

TodoList.propTypes = {};

const TodoListWrapper = styled.div`
  max-width: 87%;
  margin: 0 auto;
  padding: 3rem;
`;
export default TodoList;
