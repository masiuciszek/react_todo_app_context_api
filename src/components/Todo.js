import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Delete, Edit, Done } from 'styled-icons/material';
import { TodoContext } from '../context/todo.context';
import { REMOVE, TOGGLE } from '../context/types';
import useToggleState from '../hooks/useToggState';
import EditForm from './EditForm';

function Todo({ todo: { id, task, completed } }) {
  const [isEditing, toggle] = useToggleState();
  const { dispatch } = useContext(TodoContext);
  return (
    <TodoWrapper>
      <div className="todo">
        {isEditing ? (
          <EditForm task={task} id={id} toggle={toggle} />
        ) : (
          <h4 className={completed ? 'completed' : ''}>
            {task}{' '}
            <span>
              <Delete
                size="34"
                onClick={() => dispatch({ type: REMOVE, payload: id })}
              />
            </span>
            <span>
              <Edit size="34" onClick={toggle} />
            </span>
            <span onClick={() => dispatch({ type: TOGGLE, payload: id })}>
              <Done size="34" />
            </span>
          </h4>
        )}
      </div>
    </TodoWrapper>
  );
}

Todo.propTypes = {
  todo: PropTypes.object.isRequired,
};

const TodoWrapper = styled.div`
  padding: 1rem;
  background: #9f5afd;
  display: flex;
  width: 60%;
  margin: 0 auto;
  margin-top: 0.7rem;
  box-shadow: 1px 2px 3px 2px rgba(3, 3, 3, 0.7);
  transition: all 0.2s ease-in-out;
  &:hover {
    box-shadow: 3px 3px 4px 4px rgba(3, 3, 3, 0.7);
  }
  .todo {
    font-size: 2rem;
    background: #dcc6e0;
    width: 100%;
    padding: 2rem;
    color: #333;
    span {
      float: right;
      cursor: pointer;
    }
    .completed {
      text-decoration: line-through;
    }
  }
`;
export default Todo;
