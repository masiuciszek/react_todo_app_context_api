import React from 'react';
import './App.css';
import { TodoProvider } from './context/todo.context';
import TodoList from './components/TodoList';
import TodoForm from './components/TodoForm';
import Navbar from './components/Navbar';

function App() {
  return (
    <TodoProvider>
      <Navbar />
      <TodoForm />
      <TodoList />
    </TodoProvider>
  );
}

export default App;
