import uuid from 'uuid/v4';

export default [
  {
    id: uuid(),
    task: 'Clean my room',
    completed: false,
  },
  {
    id: uuid(),
    task: 'Go out with the dog',
    completed: false,
  },
  {
    id: uuid(),
    task: 'Grocery shopping',
    completed: false,
  },
];
