/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import React, { useReducer, createContext } from 'react';
import todosDb from '../todosDb';
import TodoReducer from './todo.reducer';

import { ADD_TODO, REMOVE, TOGGLE, EDIT } from './types';

export const TodoContext = createContext();

export function TodoProvider(props) {
  const initialState = todosDb;

  const [state, dispatch] = useReducer(TodoReducer, initialState);

  return (
    <TodoContext.Provider
      value={{
        state,
        dispatch,
      }}
    >
      {props.children}
    </TodoContext.Provider>
  );
}
