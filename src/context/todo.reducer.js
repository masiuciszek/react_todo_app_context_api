import uuid from 'uuid/v4';
import { ADD_TODO, REMOVE, TOGGLE, EDIT } from './types';

export default (state, action) => {
  const { type, payload } = action;
  console.log(state, action);
  switch (type) {
    case ADD_TODO:
      return [...state, { id: uuid(), task: payload, completed: false }];
    case REMOVE:
      return state.filter(todo => todo.id !== payload);
    case TOGGLE:
      return state.map(todo =>
        todo.id === payload ? { ...todo, completed: !todo.completed } : todo
      );
    case EDIT:
      return state.map(todo =>
        todo.id === payload.id ? { ...todo, task: payload.value } : todo
      );
    default:
      return state;
  }
};
